<?php

class UserController extends Controller
{
    protected $_message = '';
    /**
     * Метод регистрации пользователя
     */
    public function actionRegister()
    {
        $model = new User;

        $this->performAjaxValidation($model, 'form-register');

        if (Yii::app()->request->isPostRequest && isset($_POST['User'])) {

            $model->attributes = $_POST['User'];

            if ($model->save()) {

                $credit = new Credit();
                $credit->createClaim($model->id);

                $model->unsetAttributes();

                $this->_message = 'Регистрация прошла успешно!';
            }
        }

        $this->render('register', [
            'model' => $model,
            'message' => $this->_message,
        ]);

    }

    /**
     * Валидация Ajax запросов
     *
     * @param $model
     * @param $form_id
     */
    public function performAjaxValidation($model, $form_id)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']===$form_id)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}