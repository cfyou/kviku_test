<?php

/**
 * Модель для таблицы "{{user}}"
 *
 * @property integer $id
 * @property string $first_name
 * @property string $surname
 * @property string $patronymic
 * @property string $birthday
 * @property integer $passport_number
 * @property string $email
 * @property integer $phone
 * @property string $created_at
 */
class User extends CActiveRecord
{
    public $birthday_min;
    public $birthday_max;

    public function init()
    {
        $this->birthday_max = date('Y-m-d', strtotime('-18 years'));
        $this->birthday_min = date('Y-m-d', strtotime('-60 years'));
    }

    public function tableName()
    {
        return '{{user}}';
    }

    public function rules()
    {
        return [
            ['first_name, surname, patronymic, birthday, passport_number, email, phone', 'required'],
            ['first_name, surname, patronymic, email', 'length', 'max' => 64],
            ['first_name, surname, patronymic', 'onlyCirillicValidate'],
            ['email', 'email'],
            ['birthday', 'type', 'type' => 'date', 'dateFormat' => 'yyyy-MM-dd'],
            ['birthday', 'birthdayValidate'],
            ['passport_number', 'passportNumberValidate'],
            ['phone', 'phoneValidate'],
            ['phone, passport_number', 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'first_name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'birthday' => 'Дата рождения',
            'passport_number' => 'Серия и номер паспорта',
            'email' => 'E-mail',
            'phone' => 'Номер телефона',
        ];
    }

    /**
     * Метод валидации поля на кириллицу
     * Запрещает все символы кроме алфавита кириллицы
     *
     * @param $attribute
     * @param $params
     */
    public function onlyCirillicValidate($attribute, $params)
    {
        $this->$attribute = preg_replace('/\s+/ui', '', $this->$attribute);

        if (!preg_match('/^[а-яА-Я]+$/ui', $this->$attribute))
        {
            $this->addError($attribute, 'Разрешены только русские символы.');
        }
    }

    /**
     * Метод валидации даты рождения
     *
     * @param $attribute
     * @param $params
     */
    public function birthdayValidate($attribute, $params)
    {
        if ($this->birthday > $this->birthday_max || $this->birthday < $this->birthday_min)
        {
            $this->addError('birthday', 'Запрещено лицам младше 18 и старше 60.');
        }
    }

    /**
     * Метод валидации серии и номера паспорта
     *
     * @param $attribute
     * @param $params
     */
    public function passportNumberValidate($attribute, $params)
    {
        $passport_number = preg_replace('/\s+/', '', $this->passport_number);

        if (is_numeric($passport_number) && strlen($passport_number) == 10) {
            $this->passport_number = (int)$passport_number;
            return;
        }

        $this->addError('passport_number', 'Серия и номер паспорта не верны.');
    }

    /**
     * Метод валидации номера телефона
     *
     * @param $attribute
     * @param $params
     */
    public function phoneValidate($attribute, $params)
    {
        $phone = preg_replace('/\s+/', '', $this->phone);

        if (preg_match('/\+7[0-9]{10}/', $phone)) {
            $this->phone = $phone;
            return;
        }

        $this->addError('phone', 'Номер телефона введен не верно.');
    }
}
