<?php

/**
 * Модель для таблицы "{{credit_claim}}"
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $sum
 * @property integer $interest_rate
 * @property string $user_agent
 * @property integer $ip_address
 * @property string $created_at
 */
class CreditClaim extends CActiveRecord
{
	public function tableName()
	{
		return '{{credit_claim}}';
	}

	public function rules()
	{
		return [
			['user_id', 'required'],
			['user_id, sum, interest_rate', 'numerical', 'integerOnly'=>true],
			['user_agent', 'length', 'max'=>512],
			['ip_address', 'length', 'max'=>40],
		];
	}
}
