<?php

/**
 * Класс для управления кредитами
 */
class Credit
{
    const CREDIT_SUM = 50000;

    /**
     * Метод создания заявки на кредит
     *
     * @param $user_id
     */
    public function createClaim($user_id)
    {
        $model = new CreditClaim();

        $model->attributes = [
            'user_id' => $user_id,
            'sum' => self::CREDIT_SUM,
            'interest_rate' => Yii::app()->params['interestRate'],
            'user_agent' => Yii::app()->request->userAgent,
            'ip_address' => Yii::app()->request->userHostAddress,
        ];

        $model->save();
    }
}