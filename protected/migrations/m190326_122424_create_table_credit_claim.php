<?php

class m190326_122424_create_table_credit_claim extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_credit_claim', array(
            'id' => 'pk',
            'user_id' => 'INT(11) NOT NULL DEFAULT 0',
            'sum' => 'INT(11) NOT NULL DEFAULT 0',
            'interest_rate' => 'TINYINT(3) NOT NULL DEFAULT 0',
            'user_agent' => 'VARCHAR(512) NOT NULL DEFAULT ""',
            'ip_address' => 'VARCHAR(40) NOT NULL DEFAULT 0',
            'created_at' => 'DATETIME NOT NULL DEFAULT now()',
        ));
	}

	public function down()
	{
        $this->dropTable('tbl_credit_claim');
	}
}