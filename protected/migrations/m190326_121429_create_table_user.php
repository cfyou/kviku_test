<?php

class m190326_121429_create_table_user extends CDbMigration
{
    public function up()
    {
        $this->createTable('tbl_user', array(
            'id' => 'pk',
            'first_name' => 'VARCHAR(64) NOT NULL DEFAULT ""',
            'surname' => 'VARCHAR(64) NOT NULL DEFAULT ""',
            'patronymic' => 'VARCHAR(64) NOT NULL DEFAULT ""',
            'birthday' => 'DATETIME NOT NULL',
            'passport_number' => 'INT(10) NOT NULL DEFAULT 0',
            'email' => 'VARCHAR(64) NOT NULL DEFAULT ""',
            'phone' => 'VARCHAR(12) NOT NULL DEFAULT 0',
            'created_at' => 'DATETIME NOT NULL DEFAULT now()',
        ));
    }

    public function down()
    {
        $this->dropTable('tbl_user');
    }
}