<?php

$this->pageTitle = Yii::app()->name . ' - Error';
?>
<div class="d-flex align-items-center justify-content-center mt-5">
    <div class="text-center">
        <h2>Ошибка <?= $code ?></h2>

        <div class="error">
            <?= CHtml::encode($message) ?>
        </div>
    </div>
</div>
