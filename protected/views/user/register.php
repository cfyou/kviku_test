<?php
$this->pageTitle=Yii::app()->name . ' - Регистрация';
?>

<div class="d-flex align-items-center justify-content-center">
    <div class="form border rounded form-group">
        <?php $form = $this->beginWidget('CActiveForm', [
            'id'=>'form-register',
            'enableAjaxValidation'=>true,
        ]); ?>

        <div class="text-center mt-3"><?= CHtml::encode($message) ?></div>

        <div class="row">
            <?= $form->labelEx($model, 'first_name') ?>
            <?= $form->textField($model, 'first_name', ['class' => 'form-control']) ?>
            <?= $form->error($model, 'first_name', ['class' => 'text-danger']) ?>
        </div>

        <div class="row">
            <?= $form->labelEx($model, 'surname') ?>
            <?= $form->textField($model, 'surname', ['class' => 'form-control']) ?>
            <?= $form->error($model, 'surname', ['class' => 'text-danger']) ?>
        </div>

        <div class="row">
            <?= $form->labelEx($model, 'patronymic') ?>
            <?= $form->textField($model, 'patronymic', ['class' => 'form-control']) ?>
            <?= $form->error($model, 'patronymic', ['class' => 'text-danger']) ?>
        </div>

        <div class="row">
            <?= $form->labelEx($model, 'birthday') ?>
            <?= $form->dateField($model, 'birthday', [
                'min' => date('Y-m-d', strtotime($model->birthday_min)),
                'max' => date('Y-m-d', strtotime($model->birthday_max)),
                'class' => 'form-control'
            ]) ?>
            <?= $form->error($model, 'birthday', ['class' => 'text-danger']) ?>
        </div>

        <div class="row">
            <?= $form->labelEx($model, 'passport_number') ?>
            <?php $this->widget('CMaskedTextField', [
                'model' => $model,
                'attribute' => 'passport_number',
                'mask' => '9999 999999',
            ]); ?>
            <?= $form->error($model, 'passport_number', ['class' => 'text-danger']) ?>
        </div>

        <div class="row">
            <?= $form->labelEx($model, 'email') ?>
            <?= $form->textField($model, 'email', ['class' => 'form-control']) ?>
            <?= $form->error($model, 'email', ['class' => 'text-danger']) ?>
        </div>

        <div class="row">
            <?= $form->labelEx($model, 'phone') ?>
            <?php $this->widget('CMaskedTextField', [
                'model' => $model,
                'attribute' => 'phone',
                'mask' => '+79999999999',
            ]); ?>
            <?= $form->error($model, 'phone', ['class' => 'text-danger']) ?>
        </div>

        <div class="row submit">
            <?php echo CHtml::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary']); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>