<?php

return [
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Kviku test',
    'language' => 'ru',

    'defaultController' => 'user/register',

    'preload' => [
        'log'
    ],

    'import' => [
        'application.models.*',
        'application.components.*',
    ],

    'modules' => [
        'api' => [
        ],
        'gii' => [
            'class' => 'system.gii.GiiModule',
            'password' => 'GF3rGa',
            'ipFilters' => ['127.0.0.1', '::1'],
        ],
    ],

    'components' => [
        'urlManager' => [
            'urlFormat' => 'path',
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],

        'db' => require(dirname(__FILE__) . '/database.php'),

        'errorHandler' => [
            // use 'site/error' action to display errors
            'errorAction' => YII_DEBUG ? null : 'main/error',
        ],

        'log' => [
            'class' => 'CLogRouter',
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ],
            ],
        ],
    ],

    'params' => [

        // процентная ставка по кредиту
        'interestRate' => 10,
    ],
];
