<?php

return [
    'connectionString' => 'mysql:host=localhost;dbname=kviku',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'tablePrefix' => 'tbl_',
];