<?php

class ApiController extends CController
{
    public function init() {

        // отключаем для API все HTTP методы, кроме POST
        if (!Yii::app()->request->isPostRequest) {
            Yii::app()->end();
        }
    }

    /**
     * Метод возвращает JSON массив со статусом
     * успешного завершения метода
     *
     * @param array $response
     * @return bool
     */
    protected function success($response = [])
    {
        $response['response_status'] = 'success';

        echo json_encode($response, JSON_UNESCAPED_UNICODE);

        return true;
    }

    /**
     * Метод возвращает JSON массив со статусом ошибки
     *
     * @param $message
     * @param array $extra
     * @return bool
     */
    protected function error($message, $extra = [])
    {
        $response = [
            'response_status' => 'error',
            'message' => $message,
        ];

        if (count($extra) > 0) {
            $response['extra'] = $extra;
        }

        echo json_encode($response, JSON_UNESCAPED_UNICODE);

        return true;
    }
}