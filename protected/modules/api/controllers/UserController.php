<?php

class UserController extends ApiController
{
    public function actionRegister()
    {
        $model = new User();
        $model->attributes = $_POST;

        if (!$model->save()) {

            return $this->error('Возникла ошибка при регистрации пользователя', $model->getErrors());
        }

        $credit = new Credit();
        $credit->createClaim($model->id);

        return $this->success([
            'user_id' => $model->id
        ]);


    }
}